$(function(){
  $(".category_dd").on('click', 'li a', function(){
      $(".category_btn:first-child").text($(this).text());
      $(".category_btn:first-child").val($(this).text());
	  if($('.category').val() != $(this).text())
	  {
		  $('#product').val('');
	  }
	  $(".category").val($(this).text());
   });
   $(".location_dd").on('click', 'a li', function(){
      $(".location_btn").text($(this).text());
      $("#location").val($(this).text());
	  $.ajax({
					url : "setLocation.php",
					method: "post",
					data: { //search params
					   	city: $("#location").val()
					},
					success: function( data ) {
						console.log('Success');
						location.reload();
					},
					error: function( data ){
						console.log('Something went wrong, Please contact admin');
					}
					
			});
	  
   });
});

//Form1 - header form validation
function frm1chk()
{
	if(jQuery.trim($(".category_btn").text().toString()) == "Category")
	{
		$(".msgNote").html("<p><strong>Error: </strong> Please select a category</p>");
		$('.msgNote').addClass('nInformation');
		$('#msgModal').modal('show');
		return false;
	}
	return true;
}

//
$(function(){
	$("#bid-btn").click(function(e) {
		$('#loginBtn').data('src',"bid-btn");
		e.preventDefault();	
		e.stopPropagation();
		$.ajax({
            url: 'checkSession.php',
			type: 'POST',
			dataType:"json",
            success: function(data) {
                if(data.logged_in == "false")
					$("#loginModal").modal('show');
				else
					window.location = "bid_your_price.php";
            }
         });		
    });
	$("#shop-btn").click(function(e) {
		$('#loginBtn').data('src',"shop-btn");
		e.preventDefault();	
		e.stopPropagation();
		$.ajax({
            url: 'checkSession.php',
			type: 'POST',
            success: function(data) {
                if(data == "false")
					$("#loginModal").modal('show');
				else
					window.location = "shop.php";
            }
         });		
    });
});
function login() {
	var src = $('#loginBtn').data('src');
	var nw = window.open("login/index.php?src="+encodeURI(src), "Login_by_facebook", "width=600, height=450, left=400, top=110");
	$("#loginModal").modal('hide');
}
function logout(){
	var req = $('#logoutBtn').data('required');
	var red = $('#logoutBtn').data('redirect');
	if(req == true)
	{
		$('#logoutModal').modal('show');
	}
	else
	{
		window.location = 'logout.php?redirect='+red;
	}
}


function insertParam(key, value, url)
{
    key = encodeURI(key); value = encodeURI(value);

    var kvp = url.split('&');

    var i=kvp.length; var x; while(i--) 
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    return kvp.join('&'); 
}
function deleteParam(key, url)
{
    key = encodeURI(key);

    var kvp = url.split('&');

    var i=kvp.length; var x; while(i--) 
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = "";
            kvp[i] = "";
            break;
        }
    }

    
    //this will reload the page, it's likely better to store this until finished
    return kvp.join('&'); 
}



/* Get City of the user*/
/*var geocoder;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
} 
//Get the latitude and the longitude;
function successFunction(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng)
}

function errorFunction(){
    console.log("Geocoder failed");
	$.ajax({
					url : "setLocation.php",
					method: 'post',
					data: { //search params
					   	city: 'Pune'
					},
					success: function( data ) {
						console.log('Success');
					},
					error: function( data ){
						console.log('Something went wrong, Please contact admin');
					}
					
			});
}

/*  function initialize() {
    geocoder = new google.maps.Geocoder();
  }

  function codeLatLng(lat, lng) {

    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted addres
        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "locality") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }
		
        //city data
        console.log(city.long_name);
		var cityName = city.long_name;
		$.ajax({
					url : "setLocation.php",
					method: 'post',
					data: { //search params
					   	city: cityName
					},
					success: function( data ) {
						$('.location_btn').val(data);
						$('.location_btn').text(data);
						$('#location').val(data);
					},
					error: function( data ){
						console.log('Something went wrong, Please contact admin');
					}
					
			});
			
        } else {
          console.log("No results found");
		  $.ajax({
					url : "setLocation.php",
					method: 'post',
					data: { //search params
					   	city: 'Pune'
					},
					success: function( data ) {
						$('.location_btn').val(data);
						$('.location_btn').text(data);
						$('#location').val(data);
					},
					error: function( data ){
						console.log('Something went wrong, Please contact admin');
					}
					
			});
        }
      } else {
        console.log("Geocoder failed due to: " + status);
		$.ajax({
				url : "setLocation.php",
				method: 'post',
				data: { //search params
				   	city: 'Pune'
				},
				success: function( data ) {
					$('.location_btn').val(data);
					$('.location_btn').text(data);
					$('#location').val(data);
				},
				error: function( data ){
					console.log('Something went wrong, Please contact admin');
				}
					
			});
      }
    });
  }*/