/* email regex check*/
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

/* set category from category dropdown of form*/
$(function(){
  $(".category_dd2").on('click', 'li a', function(){
      $(".category_btn2:first-child").text($(this).text());
      $(".category_btn2:first-child").val($(this).text());
	  if($('.category2').val() != $(this).text())
	  {
		  $('#product2').val('');
	  }
	  $(".category2").val($(this).text());
   });
});

$(document).ready(function(e) {
	/* attach a submit handler to the form */
    $("#nyopform").submit(function(event) {
	  /* stop form from submitting normally */
      event.preventDefault();
		/* check all details are filled*/
		if($('#productid').val()=="" || $('#price').val()=="" || $('#phone').val()=="" || $('#email').val()=="" )
		{
			/* give error message if details are not filled */			
				$('.nyopNote').html("<p><strong>ERROR: </strong>Please provide all the information</p>");
				$('.nyopNote').removeClass('nSuccess');
				$('.nyopNote').addClass('nInformation');
		}
		
		/* check if email is in right pattern or not */
		else if( !isValidEmailAddress( $('#email').val() ) ) {
			/* give error message if email is not appropriate  */ 
			$('.nyopNote').html("<p><strong>ERROR: </strong>Please enter an appropriate email address</p>");
				$('.nyopNote').removeClass('nSuccess');
				$('.nyopNote').addClass('nInformation');
		}
		
		else{
      
			/* get some values from elements on the page: */
			var $form = $( this ),
			url = $form.attr( 'action' );

    	  /* Send the data using post */
      		/*var posting = $.post( url, { product: $('#product2').val(), 
		  	productid: $('#productid').val(),
			price: $('#price').val(),
	  		phone:$('#phone').val(),
	  		email:$('#email').val() } );*/

			/* change button text to loading */
			//$('#nyopbtn').val("Loading...");
		
      		/* Alerts the results */
      		/*posting.done(function( data ) {
	    		$('.veriNote').html("<p><strong>Success: </strong>Please Enter verification code sent to you through email.</p>");
				$('.veriNote').removeClass('nInformation');		
				$('.veriNote').addClass('nSuccess');
				$('#nyopbtn').val("Submit");
				$('#priceModal').modal('hide');
				$('#verificationModal').modal('show');
      		});*/
			
			$('#nyopbtn').val("Loading...");
			
			$.ajax({
					url : url,
					method: 'post',
					data: { //search params
					   	product: $('#product2').val(), 
		  				productid: $('#productid').val(),
						price: $('#price').val(),
	  					phone:$('#phone').val(),
				  		email:$('#email').val()
					},
					success: function( data ) {
						$('.veriNote').html("<p><strong>Success: </strong>Please enter the verification code we sent to your email id.</p>");
						$('.veriNote').removeClass('nInformation');		
						$('.veriNote').addClass('nSuccess');
						$('#nyopbtn').val("Submit");
						$('#priceModal').modal('hide');
						$('#verificationModal').modal('show');
					},
					error: function( data ){
						$('#nyopbtn').val("Submit");
						alert('Something went wrong, Please contact admin');
					}
					
			});
		}
   });
    
});

