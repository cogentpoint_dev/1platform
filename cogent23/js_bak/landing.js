$(document).ready(function(e) {
  $(function(){
	  $(".category-2_dd").on('click', 'li a', function(){
	      $(".category-2-btn:first-child").text($(this).text());
	      $(".category-2-btn:first-child").val($(this).text());
		  $(".category2").val($(this).text());
		  var cat = $(this).text();
		  $.ajax({
				url: 'get_brand.php',
				type: 'POST',
				data: {cat:cat},
				success:function(data){
				
					$('.brand_dd').html(data);
				}
		  });
		  $(".category-2_dd").focus();
		  $(".brand-btn:first-child").text('');
	      $(".brand-btn:first-child").val('');
		  $(".brand").val('');
	  
	   });
	   $(".brand_dd").on('click', 'li a', function(){
	      $(".brand-btn:first-child").text($(this).text());
	      $(".brand-btn:first-child").val($(this).text());
		  $(".brand").val($(this).text());
	   });
	   $(".retailers_dd").on('click', 'li a', function(){
	      $(".retailers-btn:first-child").text($(this).text());
    	  $(".retailers-btn:first-child").val($(this).text());
		  $(".retailer").val($(this).data('id'));
	   });   
	});
});
//Form2 - Bottom form validation
function frm2chk()
{
	if(jQuery.trim($(".category-2-btn").text().toString()) == "Category" || jQuery.trim($(".brand-btn").text().toString()) == "Select Category First")
	{
		$(".msgNote").html("<p><strong>Error: </strong> Please select a category</p>");
		$('.msgNote').addClass('nInformation');
		$('#msgModal').modal('show');
		return false;
	}
	return true;
}