<?php

require_once("connec.php");

$cat = "SELECT DISTINCT prdcat FROM product";
$res_cat = mysql_query($cat);

if (!$res_cat) {
    echo "Could not successfully run query ($sql) from DB: " . mysql_error();
    exit;
}

if (mysql_num_rows($res_cat) == 0) {
    echo "No rows found, nothing to print so am exiting";
    exit;
}

$rows_cat  = array();
while ($row_cat = mysql_fetch_assoc($res_cat)) {
	$rows_cat[]=$row_cat;
}
echo json_encode($rows_cat);
mysql_free_result($res_cat);

?>