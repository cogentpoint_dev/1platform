<?php
if(session_id() == '') {
    session_start();
}
if(isset($_GET['src']) && $_GET['src']!="")
{
		$_SESSION['src'] = $_GET['src'];		
}
include_once("config.php");
include_once("includes/functions.php");
if(isset($_SESSION['customer_id']))
{
	echo "<script type=\"text/javascript\" charset=\"utf-8\">window.opener.location.reload();window.self.close();</script>";
}
else{
	
if(!$fbuser) {
	$fbuser = null;
	$loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
	$output = '<script type="text/javascript">window.location="'.$loginUrl.'"</script>';
} else {
	$user_profile = $facebook->api('/me?fields=id,first_name,last_name,timezone,age_range,link,gender,email');
	$user = new Users();
	$user_data = $user->checkUser($user_profile['id'], $user_profile['first_name'], $user_profile['last_name'], $user_profile['gender'], $user_profile['timezone'], $user_profile['age_range'], $user_profile['link'],$user_profile['email']);
	if(!empty($user_data)) {
		$_SESSION["customer_id"] = $user_data['custid'];
		$_SESSION["customer_name"] = $user_profile['first_name']." ".$user_profile['last_name'];
		$_SESSION["customer_fname"] = $user_profile['first_name'];
		$_SESSION["customer_lname"] = $user_profile['last_name'];
		$_SESSION["customer_email"] = $user_profile['email'];
		if(isset($_SESSION['src']))
		{
			echo "<script type=\"text/javascript\" charset=\"utf-8\">window.opener.location.reload(); window.opener.$('#".$_SESSION['src']."').trigger('click'); window.self.close();</script>";
			unset($_SESSION['src']);
		}
		else
		{
			echo "<script type=\"text/javascript\" charset=\"utf-8\">window.opener.location.reload();window.self.close();</script>";
		}
	} else {
		$output = '<h3>Something Is Wrong...<br/>Please Try Again...!</h3>';
		$op = "";
	}
}
}
?>
<html>
<body>
<center><b><br/>

<div>
<?php
if(isset($output)){
echo $output;
}
?>
</div>

<br/></b></center>
</body>
</html>