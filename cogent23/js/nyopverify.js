$(document).ready(function(e) {
/* attach a submit handler to the form */
    $("#veriform").submit(function(event) {
      /* stop form from submitting normally */
      event.preventDefault();

      /* get some values from elements on the page: */
      var $form = $( this ),
          url = "services/insert_bid.php";

      /* Send the data using post */
      /*var posting = $.post( url, { code: $('#code').val()} );

		console.log("nyop: "+posting);
		$('#veribtn').val("Loading...");*/
      /* Alerts the results */
      /*posting.done(function( data ) {
        console.log("Data: "+data);
		if(jQuery.trim(String(data)) === "Success")
		{
			$('.nyopNote').html("<p><strong>Confirmation: </strong>Thank you for your inquiry. Your bid request has been submitted to all the sellers who have this item available. We will notify you via email as soon as we hear back from any sellers.</p>");
			$('#veribtn').val("Submit");
			$('.steps .first').addClass('done');
			$('.nyopNote').removeClass('nInformation');
			$('.nyopNote').addClass('nSuccess');
			$('#verificationModal').modal('hide');
			$('#nyopform').trigger('reset');
		}
		else if(jQuery.trim(String(data)) === "no store")
		{
			$('.nyopNote').html("<p><strong>Confirmation: </strong>Thank you for your inquiry. Your bid request has been recorded. However, this item is not available in stock with any of our sellers. Be assured, our team is checking with all the sellers and will notify you via email as soon as we hear back from any sellers.</p>");
			$('#veribtn').val("Submit");
			$('.nyopNote').removeClass('nInformation');
			$('.nyopNote').addClass('nSuccess');
			$('#verificationModal').modal('hide');
			$('#nyopform').trigger('reset');			
		}
		else
		{
			$('#veribtn').val("Submit");
			$('.veriNote').html("<p><strong>ERROR: </strong>The verification code you entered is incorrect. Please re-enter the verification code.</p>");
			$('.veriNote').removeClass('nSuccess');
			$('.veriNote').addClass('nInformation');
		}
      });*/
	  $('#veribtn').val("Loading...");
	  //var id = $('#productid').val();
	  $.ajax({
			url : url,
			method: 'post',
			data: { 
			   	code: $('#code').val()
			},
			success: function( data ) {
				console.log("data: "+data);
				if(jQuery.trim(String(data)) === "Success")
				{
					$('.nyopNote').html("<p><strong>Confirmation: </strong>Thank you for your inquiry. Your bid request has been submitted to all the sellers who have this item available. We will notify you via email as soon as we hear back from any sellers.</p>");
					$('#veribtn').val("Submit");
					$('.steps .first').addClass('done');
					$('.nyopNote').removeClass('nInformation');
					$('.nyopNote').addClass('nSuccess');
					$('#verificationModal').modal('hide');
					//$("#buy-"+id).attr("disabled","disabled");
					//$("#bid-price-"+id).attr("disabled","disabled");
					
					if(($("#priceModal").data('bs.modal') || {}).isShown==false){
						if($('myproduct').attr('display')!='none'){	
							$('#priceModal').modal('show');
							$('#priceModal').find('.nyopform').hide();
							$('#priceModal').find('.nyopNote').show();
						}
					}
					$('.border .nyopform').hide();
					$('.nyopform').trigger('reset');
					$("#veriform").trigger('reset');
				}
				else if(jQuery.trim(String(data)) === "nostore")
				{
					$('.nyopNote').html("<p><strong>Confirmation: </strong>Thank you for your inquiry. Your bid request has been recorded. However, this item is not available in stock with any of our sellers. Be assured, our team is checking with all the sellers and will notify you via email as soon as we hear back from any sellers.</p>");
					$('#veribtn').val("Submit");
					$('.nyopNote').removeClass('nInformation');
					$('.nyopNote').addClass('nSuccess');
					$('#verificationModal').modal('hide');
					if(($("#priceModal").data('bs.modal') || {}).isShown==false){
						if($('myproduct').attr('display')!='none'){	
							$('#priceModal').modal('show');
							$('#priceModal').find('.nyopform').hide();
							$('#priceModal').find('.nyopNote').show();

						}
					}
					$('.border .nyopform').hide();
					$('.nyopform').trigger('reset');
					$("#veriform").trigger('reset');			
				}
				else
				{
					$('#veribtn').val("Submit");
					$('.veriNote').html("<p><strong>ERROR: </strong>The verification code you entered is incorrect. Please re-enter the verification code.</p>");
					$('.veriNote').removeClass('nSuccess');
					$('.veriNote').addClass('nInformation');
				}
			},
			error: function( data ){
				$('#veribtn').val("Submit");
				alert('Something went wrong, Please contact admin');
			}
				
		});
    });    
});
