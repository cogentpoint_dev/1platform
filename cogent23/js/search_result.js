flag = true; // check code is busy or not
no_data = true; // is data exists then true
$(document).ready(function(e) {
	getProduct(); 
	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() == $(document).height()){
			$("#loader").show(); //show loader
			first = $('#first').val(); //get initial product numbers
			limit = $('#limit').val(); //get limit to display next
			if(flag && no_data){
				flag = false; //code is now busy
				$('#loader').show(); //show loader
				$.ajax({
					url : 'ajax_product.php',
					method: 'post',
					data: { //search params
					   	start: $('#first').val(),
						limit: 20,
						cat: $('#cat').val(),
						retailer: $('#store').val(),
						brand: $('#brand').val(),
						location: $('#location').val()
					},
					success: function( data ) {
						flag = true; //code has done it's work
						$('#loader').hide(); //hide loader
						if(data !=''){
							first = parseInt($('#first').val());
							limit = parseInt($('#limit').val());
							$('#first').val( first+limit ); //get total present products and place it
							$('.wrap').append(data); //append new products to div
						}else{
							$("#loader").show();
							$('#loader').css('background','none');
							$('#loader').css('text-align','center');
							$('#loader').html('No more products to show');
							no_data = false;
						}
					},
					error: function( data ){
						flag = true;
						$('#loader').hide();
						no_data = false;
						alert('Something went wrong, Please contact admin');
					}
					
				});
			}
			
		}
	});
	
	
	
	$('.btn-fav').on('click',function(){
		id = $(this).data('id');
		$.ajax({
            url: 'checkSession.php', //check if user is logged in or not
			type: 'POST',
			dataType:"json",
            success: function(data) {
                if(data.logged_in == "false")
					$("#loginModal").modal('show'); //if not then make him login
				else{
					//or let him do the work
					$.ajax({
            			url: 'add_fav.php', //check if user is logged in or not
						type: 'POST',
						data:{ prdid: id },
			            success: function(data) {
            			    if(data == "Success")
								$('#fav'+id).css('background-color','#FF3');//if not then make him login
							else
								console.log(data);
            			}
			         });
				}
            }
         });
		//console.log("Buy ID: "+$(this).data('id'));
	});	
	
	
	//Filter
	$(function(){
	//change size filter
  $(".brand_dd").on('click', 'li a', function(){
      $(".brand-btn:first-child").text($(this).text());
      $(".brand-btn:first-child").val($(this).text());
	  window.location.search = insertParam("brand",$(this).text(),window.location.search);
   });
   $(".retail_dd").on('click', 'li a', function(){
      $(".retail-btn:first-child").text($(this).text());
      $(".retail-btn:first-child").val($(this).text());
	  window.location.search = insertParam("store",$(this).data('id'),window.location.search);
	  
   });
   $(".price_dd").on('click', 'li a', function(){
      $(".price-btn:first-child").text($(this).text());
      $(".price-btn:first-child").val($(this).text());
	  var url = window.location.search;
	  
	  if($(this).data('min') === undefined)
		  url = deleteParam("min",url);
	  if($(this).data('max') === undefined)
		  url = deleteParam("max",url);
	  
	  if($(this).data('min') !== undefined)
		  url = insertParam("min",$(this).data('min'),url);
	  if($(this).data('max') !== undefined)
		  url = insertParam("max",$(this).data('max'),url);
	  
	  window.location.search = url;
   });
   
   //change 
});
	
});

$(document).on('click', '.buy', function(e) {

//do whatever
	id = $(this).data('id');
		btnid=$(this).attr('id');
		console.log('id:'+id);
		$('#loginBtn').data('src',btnid);
		$.ajax({
            url: 'checkSession.php', //check if user is logged in or not
			type: 'POST',
			dataType:"json",
            success: function(data) {
                if(data.logged_in == "false")
					$("#loginModal").modal('show'); //if not then make him login
				else
					window.location = "product_main.php?product_id="+id+"&mode=buy"; //or let him do the work
            }
         });

});
function showModal(modal,id){
	console.log(id);
	$('#loginBtn').data('src',id);
		 $.ajax({
            url: 'checkSession.php', //check if user is logged in or not
			type: 'POST',
			dataType:"json",
            success: function(data) {
                if(data.logged_in == "false")
					$("#loginModal").modal('show'); //if not then make him login
				else
					$(modal).modal('show'); //or let him do the work
            }
         });
}
function getProduct(){
	$("#loader").show(); //show loader
			first = $('#first').val(); //get initial product numbers
			limit = $('#limit').val(); //get limit to display next
			if(flag && no_data){
				flag = false; //code is now busy
				$('#loader').show(); //show loader
				$.ajax({
					url : 'ajax_product.php',
					method: 'post',
					data: { //search params
					   	start: 0,
						limit: 20,
						cat: $('#cat').val(),
						retailer: $('#store').val(),
						brand: $('#brand').val(),
						location: $('#location').val()
					},
					success: function( data ) {
						flag = true; //code has done it's work
						$('#loader').hide(); //hide loader
						if(data !=''){
							first = parseInt($('#first').val());
							limit = parseInt($('#limit').val());
							$('#first').val( first+limit ); //get total present products and place it
							$('.wrap').append(data); //append new products to div
						}else{
							$("#loader").show();
							$('#loader').css('background','none');
							$('#loader').css('text-align','center');
							$('#loader').html('No more products to show');
							no_data = false;
						}
					},
					error: function( data ){
						flag = true;
						$('#loader').hide();
						no_data = false;
						alert('Something went wrong, Please contact admin');
					}
					
				});
			}
}
