// autocomplet : this function will be executed every time we change the text
$(document).ready(function(e) {
	$('#product').autocomplete({
		source: function( request, response ) {
			console.log("auto call");
  			$.ajax({
	  			url : 'ajax_refresh.php',
  				dataType: "json",
				type:"POST",
				data: {
				   keyword: $("#product").val(),
				   cat: $('.category_btn').text(),
				},
				 success: function( data ) {
				 	//debugger;
				 	//datavalue = JSON.parse(data);
					response( $.map( data, function( item ) {
						//console.log(item);
				 	//var code = item.split("|");
					//console.log('item: '+item['prdname']);
					//console.log('code: '+item['']);
					return {
						label: item['prdname'],
						value: item['prdname'],
						data : item['prdname']
					}
				}));

				}
	  		});
	  	},
	  	autoFocus: true,	      	
	  	minLength: 0,
	  	select: function( event, ui ) {
			console.log(ui.item.data);
		}		      	
	});
	    
});

$(document).ready(function(e) {
	$('#bid_page_product').autocomplete({
		source: function( request, response ) {
			console.log("auto call");
  			$.ajax({
	  			url : 'ajax_refresh.php',
  				dataType: "json",
				type:"POST",
				data: {
				   keyword: $("#bid_page_product").val(),
				   cat: $('#bid_page_category').val(),
				},
				 success: function( data ) {
				 	//debugger;
				 	//datavalue = JSON.parse(data);
					response( $.map( data, function( item ) {
						//console.log(item);
				 	//var code = item.split("|");
					//console.log('item: '+item['prdname']);
					//console.log('code: '+item['']);
					return {
						name: item['prdname'],
						id: item['prdid'],
						value : item['prdname']
					}
				}));

				}
	  		});
	  	},
	  	autoFocus: true,	      	
	  	minLength: 0,
	  	select: function( event, ui ) {
			console.log(ui.item.data);
			console.log(ui.item.value);
			console.log(ui.item.name);
			console.log(ui.item.id);
			$('#bid_productid').val(ui.item.id);
		}		      	
	});
	    
});
/*
function autocomplet(){
		console.log('auto'+$('#category').val());
	  			$.ajax({
		  			url : 'ajax_refresh.php',
	  				dataType: "json",
					type:"POST",
					data: {
					   keyword: request.term,
					   cat: $('#category').val(),
					},
					 success: function( data ) {
						response( $.map( data, function( item ) {
					 	var code = item.split("|");
						console.log('item: '+item);
						console.log('code: '+code);
						return {
							label: code[0],
							value: code[0],
							data : item
						}
					}));

					}
		  		});
}*/
/*function autocomplet2() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#product2').val();
	var cat = $('.category2').val();
	if (keyword.length > min_length) {
		$.ajax({
			url: 'ajax_refresh2.php',
			type: 'POST',
			data: {keyword:keyword,
			cat: cat},
			success:function(data){
				$('#product_list2').show();
				$('#product_list2').html(data);
			}
		});
	} else {
		$('#product_list2').hide();
	}
}

function autohide()
{
	if(!($(this).is(':focus') || $('#product_list').is(':hover')))
	{
		$('#product_list').hide();
	}
}
// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#product').val(item);
	// hide proposition list
	$('#product_list').hide();
}
function set_item2(item,id) {
	// change input value
	$('#product2').val(item);
	$("#productid").val(id);
	// hide proposition list
	$('#product_list2').hide();
}
function showMessage(message,type)
{
	alert(message);
}*/