 $(function(){
   $(".category_dd").on('click', 'li a', function(){
      $(".category_btn:first-child").text($(this).text());
      $(".category_btn:first-child").val($(this).text());
   });
   $(".location_dd").on('click', 'li a', function(){
      $(".location_btn:first-child").text($(this).text());
      $(".location_btn:first-child").val($(this).text());
   });
   $(".category-2_dd").on('click', 'li a', function(){
      $(".category-2-btn:first-child").text($(this).text());
      $(".category-2-btn:first-child").val($(this).text());
   });
   $(".brand_dd").on('click', 'li a', function(){
      $(".brand-btn:first-child").text($(this).text());
      $(".brand-btn:first-child").val($(this).text());
   });
   $(".retailers_dd").on('click', 'li a', function(){
      $(".retailers-btn:first-child").text($(this).text());
      $(".retailers-btn:first-child").val($(this).text());
   });   
});