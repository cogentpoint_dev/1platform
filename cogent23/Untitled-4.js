$(document).ready(function(e) {
    // Facebook Login Code
	function login(){
			$.getJSON( "checkSession.php", function( data ) {
        	console.log("Session Check");
			console.log(data.logged_in);
			if(data.logged_in == "false"){
				FB.login(function(response) {
					console.log("response");
					console.log(response);
					if (response.authResponse) {
						console.log('Welcome!  Fetching your information.... ');
						
						FB.api('/me',{fields:'id,first_name,last_name,timezone,age_range,link,gender,email'}, function(response) {
						console.log("ME");
						console.log(response);
						console.log('Good to see you, ' + response.name + '.');
							$.ajax({
								method: "POST",
								url: "facebook_login.php",
								data: { fb: response }
							});
						});
						//window.location.href = "index.php";
					} 
					else {
						console.log('User cancelled login or did not fully authorize.');
					}
				}, { auth_type: 'reauthenticate' });					
			}
		});	
	}
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '458461167612306',
			xfbml      : true,
			version    : 'v2.5',
			status: true
		});
		
		
	};
	
	//Load facebook SDK With Async call
	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
});